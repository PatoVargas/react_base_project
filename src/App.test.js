import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

describe('App component', () => {
  it('Render app', () => {
    const wrapper = shallow(<App />);
    const text = wrapper.text();
    expect(text).toContain('React');
  });
});
