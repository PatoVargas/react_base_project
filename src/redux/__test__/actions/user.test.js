import { setUserLogged } from '../../actions/user';
import { USER_LOGGED } from '../../constants/user';

describe('user actions test', () => {
  it('should create an action to set user logged', () => {
    const expectedAction = {
      type: USER_LOGGED,
    };
    expect(setUserLogged()).toEqual(expectedAction);
  });
});
