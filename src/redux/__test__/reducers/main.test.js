import reducer from '../../reducers/main';

describe('user reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toHaveProperty('user');
  });
});
