import reducer from '../../reducers/user';
import { USER_LOGGED } from '../../constants/user';

const initialState = {
  userLogged: false,
};

describe('user reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle USER_LOGGED', () => {
    expect(
      reducer(initialState, {
        type: USER_LOGGED,
      }),
    ).toEqual({ userLogged: true });
  });
});
